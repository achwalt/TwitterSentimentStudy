from matplotlib import pyplot
import numpy as np
import pandas as pd
import os

from data import Data

class Plots:

    def __init__(self):
        self.export_dir = "Export"
        self.image_dir = "Plots"

    def export_plot(self, fname):
        try: os.makedirs(os.path.join(self.export_dir, self.image_dir))
        except: pass
        pyplot.savefig(os.path.join(self.export_dir, self.image_dir, fname), dpi = 400, bbox_inches = "tight")

    def item_count(self, dict_of_item_frequencies, title = "Most frequent items", fname = "", len_limit = 10):
        if len_limit:
            item_freq = {}
            keys, values = list(dict_of_item_frequencies.keys()), list(dict_of_item_frequencies.values())
            for i in range(len_limit):
                key, val = keys[i], values[i]
                item_freq[key] = val
        else: item_freq = dict_of_item_frequencies
        pyplot.figure()
        pyplot.bar(x = np.array(list(item_freq.keys())), height = np.array(list(item_freq.values())))
        pyplot.title(title)
        if fname: self.export_plot(fname)

    def histogram(self, series, title = "Histogram", bins = 20, fname = "", upper_cutoff = None, lower_cutoff = None):
        if upper_cutoff: series_ = series[series <= upper_cutoff]
        if lower_cutoff: series_ = series_[series_ >= lower_cutoff]
        else: series_ = series
        pyplot.figure()
        pyplot.hist(x = series_.values, bins = bins)
        pyplot.title(title)
        if fname: self.export_plot(fname)

if __name__ == "__main__":
    data = Data()
    data.load()
    plots = Plots()
    plots.item_count(data.count_appearance(data.src["$Ticker"]))
    plots.histogram(data.src.likeCount, bins = 20, upper_cutoff = 150)
