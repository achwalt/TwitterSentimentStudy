# Prerequisites   
*Please note: The Sentiment Model is only tested on Ubuntu 20.04*

## Installation  
- You will need Python 3.7 (breaking changes in multiprocessing to earlier versions). Pip needs to be available.  
- **Create a new conda environment** and run ```python setup.py```, which will download and install all dependencies.  
    - *setup.py* will two larger files (~ 2 GB, including pretrained models for the sentiment classifier) and store them in the folders *.data* and *.sentiments*. I highly encourage downloading these, since you need a lot of patience to reproduce them yourself (you can, though).  
    - *setup.py* will also run the command ```pip install -r requirements.txt``` for you.

A Hugging Face **Transformer Sentiment Classifier** is implemented. This is transfer learned from a BERT model implementation, which **requires NVIDIA CUDA**. If you want to train the model on your own hardware, do not download my pretrained files provided in *setup.py*. Note that BERT is HUGE (although I've opted for a small model) and might not fit in your GPU, therefore I strongly discourage you from doing so!

All data is available in the ".data" directory unless it is sentiment-related in which case you can find it in ".sentiment".

## Contents  
You just need to look at the following three files:
- **setup.py**, to get you started.
- **Solution_Benjamin_Albrechts.ipynb**, which is the executed Jupyter notebook. It is an overview of the underlying code base and a summary of the task.
- **Summary_of_Results.pdf**, which is the required 1-pager document summarizing all results.

## Working Principles
Jupyter is great for visualizing results in an orderly manner and notebooks may provide a good overview over a project. On the other hand it discourages writing working code, therefore I have put the majority of the code base into dedicated Python files and classes. This notebook is therefore a summary of the results and I will walk you through the code files if requested during the interview.
