import os

if __name__ == "__main__":
    try:
        os.system("pip install -r requirements.txt")
        print(">>> Installation complete <<<")
    except: print("'pip' not installed or not added to path. Please run 'pip install -r requirements.txt' once you have everything set up.")

import requests
import shutil
from tqdm import tqdm

def download_file_from_google_drive(id, destination):
    URL = "https://docs.google.com/uc?export=download"

    session = requests.Session()

    response = session.get(URL, params = { 'id' : id }, stream = True)
    token = get_confirm_token(response)

    if token:
        params = { 'id' : id, 'confirm' : token }
        response = session.get(URL, params = params, stream = True)

    save_response_content(response, destination)    

def get_confirm_token(response):
    for key, value in response.cookies.items():
        if key.startswith('download_warning'):
            return value

    return None

def save_response_content(response, destination):
    CHUNK_SIZE = 32768

    with open(destination, "wb") as f:
        for chunk in tqdm(response.iter_content(CHUNK_SIZE), desc = f"Downloading '{destination}'"):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)

def extract_contents(destination):
    shutil.unpack_archive(filename = destination)
    os.remove(destination)
    
if __name__ == "__main__":
    data_id = "1ztSLmRC0q5CgTgNGPHn2DggSgVt75-LP"
    data_destination = '.data.tar.xz'
    download_file_from_google_drive(data_id, data_destination)
    print(">>> Download finished. Unpacking archive...")
    extract_contents(data_destination)
    print(">>> Unpack complete.")
    
    sentiments_id = "1LT9R-R-kQEhiq9O7d2_qaqjdZI-U0-FY"
    sentiments_destination = '.sentiments.tar.xz'
    download_file_from_google_drive(sentiments_id, sentiments_destination)
    print(">>> Download finished. Unpacking archive...")
    extract_contents(sentiments_destination)
    print(">>> Unpack complete.")
    
    print(">>> Everything successfully installed.")