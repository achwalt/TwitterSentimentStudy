import pandas as pd
import pandas_datareader as pdr
from datetime import datetime
import re
import requests
import multiprocessing as mp
import yaml
from bs4 import BeautifulSoup
import os

from helpers import run_procs, load_mapping
from data import Data

class Stocks:

    def __init__(self):
        self.mapping_path = ".mapping"
        self.data_folder = ".data"
        self.stock_prices_path = "stock_prices.csv"
        self.stocktwits_symbols_lookup_url = "https://stocktwits.com/symbol/"
        self.yahoo_similar_symbols_lookup_url = "https://finance.yahoo.com/lookup?s="
        self.ticker_regex_key = "\$[A-Z]+"
    
    @staticmethod
    def datetime_to_yahoo(self, datetime_date = None):
        if not datetime_date: datetime_date = datetime.now()
        return datetime.strftime(datetime_date, format = "%Y-%m-%d")

    @staticmethod
    def escape_ticker(ticker):
        return ticker[1:] if str(ticker).startswith("$") else ticker

    @staticmethod
    def get_returns(dataframe, fillna = True):
        if fillna: return (dataframe/dataframe.shift().values).fillna(1) - 1
        else: return dataframe/dataframe.shift().values - 1

    def adjust_open(self, stock_prices):
        # Extract relevant colnames
        adj_close_prices = [i for i in stock_prices.columns if i.endswith("Adj Close")]
        close_prices = [i for i in stock_prices.columns if i.endswith("Close") and "Adj " not in i]
        open_prices = [i for i in stock_prices.columns if i.endswith("Open")]
        # Slice Dataframes
        adj_close_prices = stock_prices.loc(1)[adj_close_prices]
        close_prices = stock_prices.loc(1)[close_prices]
        open_prices = stock_prices.loc(1)[open_prices]
        
        outperformance = pd.DataFrame(self.get_returns(adj_close_prices).values - self.get_returns(close_prices).values)
        outperformance = outperformance.fillna(0)
        # Questionable assumption: 
        # The outperformance of Adj Close over Close is equal to Adj Open over Open
        # but shifted by a day
        adj_open_prices = open_prices * (1 + outperformance).cumprod().shift().fillna(1).values
        adj_open_prices.index = adj_close_prices.index
        adj_open_prices.columns = [i.split()[0] + " Adj Open" for i in open_prices.columns]
        return (adj_open_prices, adj_close_prices)
        
    def import_stocks(self, stock_list, start = None, stop = None):
        start = start if start else "2000-01-01"
        stop = stop if stop else self.datetime_to_yahoo(datetime.now())
        stock_list_ = [self.escape_ticker(i) for i in stock_list]
        df = pdr.DataReader(stock_list_, "yahoo", start = start, end = stop)
        df.columns = [f"{i[1]} {i[0]}" for i in df.columns]
        # df = df.loc(1)[[("Adj Close" in c) for c in df.columns]]
        return df

    def extract_tickers(self, string):
        return re.findall(self.ticker_regex_key, string)

    def extract_tickers_from_list(self, series_of_strings, unique = False):
        matches = []
        for string in series_of_strings:
            results = self.extract_tickers(string)
            if unique: matches.extend(results)
            else: matches.append(results)
        if unique: matches = list(set(matches))
        return matches

    def import_twitter_stocks(self, series_of_tweets, start = None, stop = None):
        stock_list = self.extract_tickers_from_list(series_of_tweets, unique = True)
        adjusted_prices = self.import_stocks(stock_list, start, stop)
        return adjusted_prices

    def lookup_similar_tickers(self, ticker):
        "Lookup company name for each ticker, then try to find a yahoo equivalent"
        headers = {"user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0"}
        url = self.stocktwits_symbols_lookup_url + ticker
        html = requests.get(url, headers = headers).text
        soup = BeautifulSoup(html, features = "lxml")
        full_company_name = soup.find_all('title', limit = 1)[0].text.replace(" — Stock Price and Discussion | Stocktwits", "")
        full_company_name = " ".join(full_company_name.split(" ")[1:])
        url =  self.yahoo_similar_symbols_lookup_url + full_company_name
        result = requests.get(url, headers = headers).text # TODO: Silence me if I fail!
        return pd.read_html(result)[0]

    def _gen_twits_yahoo_map(self, ticker, queue):
        result = self.lookup_similar_tickers(ticker)
        queue[ticker] = result.Symbol[0]

    def gen_twits_yahoo_map(self, list_of_tickers, fname = None):
        "Generate a naive mapping for stocktwits to yahoo - This is acceptable for such a bare-bone prototype"
        if not fname: fname = self.mapping_path
        queue = mp.Manager().dict()
        list_of_procs = []
        for ticker in list_of_tickers:
            list_of_procs.append(mp.Process(target = self._gen_twits_yahoo_map, args = (self.escape_ticker(ticker), queue)))
        run_procs(list_of_procs, desc = "Running Procs", limit = None, daemon = True, wait = 0.1)
        mapping = {}
        mapping["ToYahoo"] = {}
        mapping["FromYahoo"] = {}
        for key in queue.keys():
            mapping["ToYahoo"][key] = queue[key]
            mapping["FromYahoo"][queue[key]] = key
        if fname is not None: 
            with open(fname, "w", encoding = "utf-8-sig") as f:
                yaml.dump(mapping, f)
        return mapping

    def apply_map(self, list_of_tickers, mapping, yahoo = "to"):
        mapped = []
        yahoo = "ToYahoo" if yahoo == "to" else "FromYahoo"
        for ticker in list_of_tickers:
            ticker_ = self.escape_ticker(ticker)
            try: mapped.append(mapping[yahoo][ticker_])
            except KeyError: pass # TODO: Investigate what causes tickers to be unavailable
        return mapped

    def retrieve_data(self, list_of_tickers, df, fpath = None):
        if fpath == True: fpath = self.stock_prices_path
        mapping = load_mapping()
        yahoo_tickers = self.apply_map(list_of_tickers, mapping, yahoo = "to")
        stock_prices = self.import_stocks(yahoo_tickers, start = self.datetime_to_yahoo(df.date.min()), stop = self.datetime_to_yahoo(df.date.max()))
        if fpath is not None: stock_prices.to_csv(fpath, encoding = "utf-8-sig")
        return stock_prices

    def load_data(self, fpath = None):
        if not fpath: fpath = os.path.join(self.data_folder, self.stock_prices_path)
        stock_prices = pd.read_csv(fpath, index_col = 0, encoding = "utf-8-sig").dropna(how = "all").ffill()
        stock_prices.index = pd.to_datetime(stock_prices.index, format = "%Y-%m-%d")
        return stock_prices

if __name__ == "__main__":
    st = Stocks()
    """ dt = Data()
    df = dt.load()
    # list_of_tickers = st.extract_tickers_from_list(df.content, unique = True)
    # I could create a mapping for this too, but needs extensive testing which is out of scope for this initial project.
    list_of_tickers = list(set(df["$Ticker"].to_list()))[1:]
    # mapping = st.gen_twits_yahoo_map(list_of_tickers) # ONLY run if necessary. Mapping is supplied!
    stock_prices = st.retrieve_data(list_of_tickers, df) """
    spx = st.import_stocks(["^GSPC"], "2020-03-01", "2020-03-31")
    spx.to_csv(os.path.join(st.data_folder, "spx.csv"), encoding = "utf-8-sig")