import pandas as pd
import numpy as np
import re
import os
class Data:
    
    def __init__(self):
        self.regex_key = "[0-9]+-[0-9]+-[0-9]+ [0-9]+:[0-9]+:[0-9]+\+00:00"
        self.datetime_format = "%Y-%m-%d %H:%M:%S+00:00"
        self.src = None
        self.summary_statistics = {}
        self.csv_path = os.path.join(".data","sp500-mar2020.csv")
    
    @staticmethod
    def _recognize_timestamp(string, regex_key):
        try: 
            re.search(regex_key, string)[0]
            return True
        except TypeError: return False
    
    def recognize_timestamp(self, series_of_strings, regex_key = None):
        if not regex_key: regex_key = self.regex_key
        return series_of_strings.apply(lambda string: self._recognize_timestamp(string, regex_key)).values
    
    @staticmethod
    def count(array, matching_value = False):
        return len([item for item in array if item == matching_value])
    
    @staticmethod
    def count_appearance(array, sort = True):
        appearances = {}
        for item in array:
            try:
                appearances[item] += 1
            except KeyError:
                appearances[item] = 1
        if sort: appearances = {k: v for k, v in sorted(appearances.items(), reverse = True, key = lambda item: item[1])}
        return appearances
    
    def filter_bad_rows(self, dataframe):
        return dataframe[self.recognize_timestamp(dataframe.date)]
    
    def create_summary_statistics(self, data = None):
        # data.describe() is not meaningful here: Only describes scalar data and produces unaesthetically many zeros here
        # Yes, this is intentional over-engineering
        if type(data) is not pd.DataFrame: data = self.src
        
        def avoid_e(series, time_to_str = False):
            "Avoid scientific notation for better readability"
            series_ = series if not time_to_str else pd.Series(series.values.astype(np.int64) // 10 ** 9, name = "date")
            return series_.describe().apply(lambda x: format(x, 'f'))
        
        self.summary_statistics["Date"] = avoid_e(data.date, True)
        self.summary_statistics["ActiveUsers"] = pd.Series(self.count_appearance(data.user), name = "userActivity")
        self.summary_statistics["ReplyCount"] = avoid_e(data.replyCount)
        self.summary_statistics["RetweetCount"] = avoid_e(data.retweetCount)
        self.summary_statistics["LikeCount"] = avoid_e(data.likeCount)
        self.summary_statistics["QuoteCount"] = avoid_e(data.quoteCount)
        self.summary_statistics["Tickers"] = pd.Series(self.count_appearance(data.loc(1)["$Ticker"]), name = "tickers")
        return self.summary_statistics
    
    def summary(self, data = None, summary_statistics = None):
        data = data if type(data) == pd.DataFrame else self.src
        stat = summary_statistics if summary_statistics else self.summary_statistics
        stat["ActiveUsers"] = stat["ActiveUsers"].describe()
        stat["Tickers"] = stat["Tickers"].describe()
        return pd.concat([stat[key] for key in stat], axis = 1).fillna("")
    
    def load(self, csv_path = None, datetime_format = None, nan = "zeros"):
        if not csv_path: csv_path = self.csv_path
        assert type(csv_path) == str, "csv_path must be string"
        assert type(datetime_format) == str or datetime_format is None, "datetime_format must be a matching string"
        assert nan == "zeros" or nan == "drop", "nan must be either 'zeros' or 'drop'"
        data = pd.read_csv(csv_path, header = 0)
        data = data.fillna(0) if nan == "zeros" else data.dropna()
        data = data.drop_duplicates() # 4678 duplicate rows
        data = self.filter_bad_rows(data) # 7 broken rows
        if not datetime_format: datetime_format = self.datetime_format
        data.loc(1)["date"] = pd.to_datetime(data.loc(1)["date"], format = datetime_format)
        for col in ('replyCount', 'retweetCount', 'likeCount', 'quoteCount'):
            data.loc(1)[col] = data.loc(1)[col].astype(dtype = np.uint32, errors = "ignore")
        self.src = data
        return data

if __name__ == "__main__":
    data = Data()
    dataset = data.load()
    data.create_summary_statistics(dataset)
    print(data.summary(dataset))
